'use strict';

var gulp = require('gulp');


gulp.task('default', [
    'internal-sass',
    'internal-js',
    'vendor-css',
    'vendor-js',
    'watch'
]);