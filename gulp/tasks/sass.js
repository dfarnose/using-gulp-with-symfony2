'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var flatten = require('gulp-flatten');
var minifyCss = require('gulp-minify-css');
var concatCss = require('gulp-concat-css');

var config = require('../config');


gulp.task('internal-sass', function () {
    return gulp.src('./src/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(concatCss('./build/internal.css'))
        .pipe(minifyCss())
        .pipe(flatten())
        .pipe(gulp.dest(config.cssPath))
    ;
});