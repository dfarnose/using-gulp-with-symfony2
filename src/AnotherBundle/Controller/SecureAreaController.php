<?php

namespace AppBundle\Controller;

use AppBundle\Entity\BlogPost;
use AppBundle\Form\Type\BlogPostType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class SecureAreaController extends Controller
{
    /**
     * @Route("/admin", name="admin")
     */
    public function createAction(Request $request)
    {
        $blogPost = new BlogPost();
        $form = $this->createForm(new BlogPostType(), $blogPost);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($blogPost);
            $em->flush();

            $this->addFlash('success', 'Successfully posted a new blog post');
            $this->redirectToRoute('admin');
        }

        return $this->render('admin/manage.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
